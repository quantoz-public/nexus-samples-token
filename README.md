# Nexus Samples - token

This sample consists of
1. A webserver hosting an API to connect the Mobile Application and Nexus  
  [README](TokenSampleApi/README)
2. A Mobile Application  
  [README](SampleApp/README)

## Disclaimer with the Nexus token sample application

The Nexus sample application for token and stable coin business models is an open source combination of a C# backend API providing a connection to the React-Native mobile phone application for end-customers. The API is connected with the Nexus API to handle creation accounts and transactions.

The end-customer application supports the following actions
- Create a customer with a token account
- Get an overview of the account status and transactions
- Create a payment to another account
  - Scan another account of another mobile phone app user using the QR code option
- Cash out tokens

For the funding of accounts and issuing of tokens, the Nexus Portal can be used.

### MIT License
The sample application as-is should not be for production purposes, but as a proof-of-concept example or base for further or own development. Some changes are required to turn the sample application into a minimal viable and safe production product. On top of that you can develop all kind of additional customizations.

### Minimal adaptation for production
The sample application support the necessary options to implement a token based system for end-customers. The API is needed for the Nexus connection and will contain also the few customer details gathered during Mobile App Sign-up.

### Security - *IMPORTANT*
Use a different OAUTH server, to make this sample work, we use password grant, therefore the password will be send unencrypted to the authentication server, which is not adviced to use for a production system.

### Further customization options
1) The database is a simple local Sqlite db, which can easily be changed to something else.
2) The backend API also acts as the OAUTH authorization server, but the mobile app can be configured to connect to another OAUTH server.
3) Funding of the token accounts needs to be done manually using the Nexus Portal, it needs some additional effort to build in a "Top-Up" possibility, but a "Top Up" screen is added in the app as a mock-up for demonstration purposes.
4) Only 1 preconfigured token is currently supported in the mobile app.
5) Currently, the customer token account is not stored in phones encrypted keystore, but only in the phones local storage. For better protection, proprietary phone keystores should be used, and an option should be created for the user to export the account keys for back-up.


The Quantoz Nexus team can help you with advice and support.
