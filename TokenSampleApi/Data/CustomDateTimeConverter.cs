﻿using Newtonsoft.Json.Converters;

namespace TokenSampleApi.Data
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public const string DateFormatA = "yyyy-MM-ddTHH:mm:ssZ";

        public CustomDateTimeConverter()
        {
            DateTimeFormat = DateFormatA;
        }
    }

}
