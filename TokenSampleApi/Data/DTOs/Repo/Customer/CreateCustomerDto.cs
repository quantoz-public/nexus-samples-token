﻿using System.ComponentModel.DataAnnotations;

namespace TokenSampleApi.Data.DTOs.Repo.Customers
{
    public class CreateCustomerDto
    {
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9]+(\.?[0-9a-zA-Z_-])*@[a-zA-Z0-9]+[0-9a-zA-Z_-]*.[a-zA-Z]+$")]
        public string Email { get; set; }

        [Required]
        public string CryptoAddress { get; set; }
        
        // Reduce min-lenght to allow the 5-digits pin. For better security, change this number with a higher one
        [Required]
        [StringLength(255, MinimumLength = 5)]
        public string Password { get; set; }

        [Required]
        public string AnonymousRequestToken { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$")]
        public string LastName { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 8)]
        [RegularExpression(@"^[A-Z0-9]+$")]
        public string BankAccountNumber { get; set; }
    }
}
