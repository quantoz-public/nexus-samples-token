﻿using System.Collections.Generic;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.TransactionEnvelope;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment
{
    public class PostPaymentsResponse
    {
        /// <summary>
        /// The envelope containing all operations for creating the trades on the blockchain.
        /// </summary>
        public TransactionEnvelopeResponse TransactionEnvelope { get; set; }

        /// <summary>
        /// The created trades in Nexus.
        /// </summary>
        public IEnumerable<GetPaymentResponse> Payments { get; set; }
    }
}
