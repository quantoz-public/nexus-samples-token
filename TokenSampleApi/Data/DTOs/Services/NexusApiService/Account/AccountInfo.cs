﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Account
{
    public class AccountInfo
    {
        public string AccountAddress { get; set; }
        public string AccountCode { get; set; }
        public string CustomerCode { get; set; }
    }
}
