﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService
{
    public class PagedResult<T>
    {
        public int Page { get; set; }
        public int Total { get; set; }
        public int TotalPages { get; set; }
        public IDictionary<string, string> FilteringParameters { get; set; }
        public IEnumerable<T> Records { get; set; }
    }
}
