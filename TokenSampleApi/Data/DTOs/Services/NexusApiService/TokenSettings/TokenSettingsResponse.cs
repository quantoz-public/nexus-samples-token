﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.TokenSettings
{
    public class TokenSettingsResponse
    {
        public AllowedToken[] AllowedTokens { get; set; }
        public string DefaultSwapToken { get; set; }
        public string[] Signers { get; set; }
    }

    public class AllowedToken
    {
        public string TokenCode { get; set; }
        public string Status { get; set; }
    }
}
