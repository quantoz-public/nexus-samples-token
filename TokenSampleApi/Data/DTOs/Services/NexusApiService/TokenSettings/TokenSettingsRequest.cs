﻿namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.TokenSettings
{
    public class TokenSettingsRequest
    {
        public string[] AllowedTokens { get; set; }
        public string DefaultSwapToken { get; set; }
    }
}
