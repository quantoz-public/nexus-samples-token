﻿using System;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Account;

namespace TokenSampleApi.Data.DTOs.Services.NexusApiService.Customer
{
    public class PostCustomerRequest
    {
        public string CustomerCode { get; set; }
        public string CurrencyCode { get; set; }
        public string TrustLevel { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public PostAccountRequest[] Accounts { get; set; }
        public PostBankAccountRequest[] BankAccounts { get; set; }

        public PostCustomerRequest(string customerCode, string email, string cryptoAddress, string firstName, string lastName, string bankAccountNumber)
        {
            CustomerCode = customerCode;
            Email = email;
            CurrencyCode = "EUR";
            TrustLevel = "Trusted";
            FirstName = firstName;
            LastName = lastName;
            FullName = $"{firstName} {lastName}";
            Accounts = new PostAccountRequest[] {
                new PostAccountRequest
                {
                    AccountType = "TOKEN",
                    CryptoCode = "XLM",
                    CustomerCryptoAddress = cryptoAddress,
                    TokenSettings = new TokenSettings.TokenSettingsRequest
                    {
                        AllowedTokens = new string[] { "SCEUR" },
                        DefaultSwapToken = "SCEUR"
                    }
                }
            };
            BankAccounts = new PostBankAccountRequest[]
            {
                new PostBankAccountRequest
                {
                    BankAccountName = $"{firstName} {lastName}",
                    BankAccountNumber = bankAccountNumber
                }
            };
        }
    }
}
