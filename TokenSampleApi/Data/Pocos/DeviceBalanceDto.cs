﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.Pocos
{
    public class DeviceBalanceDto
    {
        public int DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceAddress { get; set; }
        public List<TokenBalanceDto> CurrentBalances { get; set; }
        public decimal? MaxCreditLimit { get; set; }
    }
}
