﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenSampleApi.Data.Pocos
{
    public class TransactionEnvelopeDto
    {
        public string TransactionEnvelope { get; set; }
        public bool SigningNeeded { get; set; }

        public TransactionEnvelopeDto(string transactionEnvelope, bool signingNeeded = true)
        {
            TransactionEnvelope = transactionEnvelope;
            SigningNeeded = signingNeeded;
        }
    }
}
