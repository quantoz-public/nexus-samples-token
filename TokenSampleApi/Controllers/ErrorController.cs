﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using static IdentityServer4.IdentityServerConstants;

namespace TokenSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [AllowAnonymous]
        [Route("/error-local-development")]
        public IActionResult ErrorLocalDevelopment(
            [FromServices] IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.EnvironmentName != "Development")
            {
                throw new InvalidOperationException(
                    "This shouldn't be invoked in non-development environments.");
            }

            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            // Errors from the NexusAPIService get thrown with a message of 3 characters
            // containing the HTTP responsecode, this is the only time such a short
            // message occurs. Therefore it can be handled separately.
            if (context.Error.Message.Length == 3)
            {
                return Problem(
                    detail: context.Error.InnerException.Message,
                    title: $"API Error - Nexus API {context.Error.Message}");
            }

            return Problem(
                detail: context.Error.StackTrace,
                title: "API Error\n" + context.Error.Message);
        }

        [AllowAnonymous]
        [Route("/error")]
        public IActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();

            // Errors from the NexusAPIService get thrown with a message of 3 characters
            // containing the HTTP responsecode, this is the only time such a short
            // message occurs. Therefore it can be handled separately.
            if (context.Error.Message.Length == 3)
            {
                return Problem(
                    detail: context.Error.InnerException.Message,
                    title: $"API Error - Nexus API {context.Error.Message}");
            }

            return Problem();
        }
    }
}
