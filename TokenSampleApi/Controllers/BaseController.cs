﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TokenSampleApi.Controllers
{
    [Authorize]
    public class BaseController : ControllerBase
    {
        private bool? _clientHasGivenName;

        public BaseController() { }

        private bool GetClientHasGiven_Name()
        {
            var ownerEmail = User.FindFirst("given_name")?.Value;

            return !string.IsNullOrWhiteSpace(ownerEmail);
        }

        protected bool ClientHasGiven_Name
        {
            get
            {
                return _clientHasGivenName ?? (_clientHasGivenName = GetClientHasGiven_Name()).GetValueOrDefault(true);
            }
        }
    }
}
