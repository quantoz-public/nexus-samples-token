﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using stellar_dotnet_sdk;
using TokenSampleApi.Data.Pocos;
using TokenSampleApi.Logic.Interfaces;

namespace TokenSampleApi.Logic
{
    public class StellarLogic : IBlockChainLogic
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _networkUrl;
        private readonly string _networkPassPhrase;
        private readonly Network _network;
        private readonly Server _server;

        public StellarLogic(
            IConfiguration configuration,
            IHttpClientFactory clientFactory)
        {
            _configuration = configuration;
            _clientFactory = clientFactory;
            _networkUrl = _configuration.GetSection("Stellar").GetValue<string>("networkUrl");
            _networkPassPhrase = _configuration.GetSection("Stellar").GetValue<string>("networkPassPhrase");
            _network = new Network(_networkPassPhrase);
            _server = new Server(_networkUrl, _clientFactory.CreateClient("HorizonClient"));
        }

        private decimal BalanceStringToDecimal(string balanceString)
        {
            return Convert.ToDecimal(balanceString, CultureInfo.InvariantCulture);
        }

        public async Task<IEnumerable<TokenBalanceDto>> GetAccountBalances(string account)
        {
            var accountResponse = await _server.Accounts.Account(account);

            return accountResponse.Balances
                .Select(balance => new TokenBalanceDto()
                {
                    Token = new Token(balance.AssetCode, balance.AssetIssuer),
                    TokenBalance = BalanceStringToDecimal(balance.BalanceString)
                });
        }

        public async Task<TokenBalanceDto> GetAccountBalance(string account, Token token)
        {
            var accountBalances = await GetAccountBalances(account);

            return accountBalances.FirstOrDefault(accountBalance => accountBalance.Token.Equals(token));
        }

        public async Task<TokenBalanceDto> GetEuroBalance(string account)
        {
            var accountBalances = await GetAccountBalances(account);

            return accountBalances.FirstOrDefault(accountBalance => accountBalance.Token.Code == Constants.TokenCode);
        }

        public async Task<string> GetLatestCursor()
        {
            var response = await _server.Transactions.Order(stellar_dotnet_sdk.requests.OrderDirection.DESC).Execute();
            return response.Records.First().PagingToken;
        }

        public async Task<List<PaymentDto>> GetAccountPayments(string account, string? fromCursor = null, string? upToCursor = null)
        {
            var requestBuilder = _server.Payments.ForAccount(account).Order(stellar_dotnet_sdk.requests.OrderDirection.DESC).Limit(200);

            if (!string.IsNullOrWhiteSpace(fromCursor) && !string.IsNullOrWhiteSpace(upToCursor))
            {
                requestBuilder.Cursor(upToCursor);
            }

            if (string.IsNullOrWhiteSpace(fromCursor))
            {
                fromCursor = "0";
            }

            var payments = new List<PaymentDto>();

            var finished = false;

            stellar_dotnet_sdk.responses.page.Page<stellar_dotnet_sdk.responses.operations.OperationResponse> result;

            try
            {
                result = (await requestBuilder.Execute());
            }
            catch
            {
                return new List<PaymentDto>();
            }

            while (!finished)
            {
                if (result.Records.Count == 0)
                {
                    finished = true;
                    break;
                }

                foreach (var record in result.Records)
                {
                    if (Convert.ToInt64(record.PagingToken) < Convert.ToInt64(fromCursor))
                    {
                        finished = true;
                        break;
                    }

                    if (record.Type != "payment")
                    {
                        continue;
                    }

                    var payment = record as stellar_dotnet_sdk.responses.operations.PaymentOperationResponse;

                    var paymentDto = new PaymentDto()
                    {
                        AssetAmount = BalanceStringToDecimal(payment.Amount),
                        AssetCode = payment.AssetCode,
                        AssetIssuer = payment.AssetIssuer,
                        CreatedAt = DateTime.ParseExact(payment.CreatedAt, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                        FromAddress = payment.From,
                        ToAddress = payment.To,
                        PagingToken = payment.PagingToken,
                        Successful = payment.TransactionSuccessful
                    };

                    payments.Add(paymentDto);
                }

                result = await result.NextPage();
            }

            return payments;
        }

        public string SignTransaction(string transactionEnvelopeBase64, string signer)
        {
            try
            {
                var privateKey = KeyPair.FromSecretSeed(signer);

                var envelope = Transaction.FromEnvelopeXdr(transactionEnvelopeBase64);

                Network.Use(_network);
                envelope.Sign(privateKey);

                return envelope.ToEnvelopeXdrBase64();
            }
            catch
            {
                throw new ArgumentException("Invalid signer supplied");
            }
        }
    }
}
