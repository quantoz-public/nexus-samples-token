﻿using System.Threading.Tasks;
using TokenSampleApi.Data.DTOs.Logic.Customers;
using TokenSampleApi.Data.DTOs.Repo.Customers;

namespace TokenSampleApi.Logic.Interfaces
{
    public interface ICustomerLogic
    {
        public Task<ConfirmCustomerDto> Create(CreateCustomerDto dto);
        public Task<ConfirmCustomerDto> Get(int deviceId);
    }
}
