﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using TokenSampleApi.Data.DTOs.Logic.Transaction;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment;
using TokenSampleApi.Data.Pocos;
using TokenSampleApi.Logic.Interfaces;
using TokenSampleApi.Services;

namespace TokenSampleApi.Logic
{
    public class TransactionLogic : ITransactionLogic
    {
        private readonly INexusApiService _nexusApiService;
        private readonly IBlockChainLogic _stellarLogic;

        public TransactionLogic(
            INexusApiService nexusApiService,
            IBlockChainLogic stellarLogic)
        {
            _nexusApiService = nexusApiService;
            _stellarLogic = stellarLogic;
        }

        public async Task<TransactionEnvelopeDto> CreatePayment(CreatePaymentDto paymentDto)
        {
            var singleOutput = new PostPaymentOutputs
            {
                Sender = paymentDto.FromAddress,
                Receiver = paymentDto.ToAddress,
                Amount = paymentDto.Amount,
                TokenCode = paymentDto.TokenCode
            };

            var pmtResponse = await _nexusApiService.CreatePayment(new PostPaymentOutputs[] { singleOutput }, paymentDto.Memo);

            return new TransactionEnvelopeDto(pmtResponse.TransactionEnvelope.SignedTransactionEnvelope);
        }

        public async Task<TransactionEnvelopeDto> CreatePayout(CreatePayoutDto payoutDto)
        {
            var accountCode = $"XLM-{payoutDto.FromAddress}";
            var amount = payoutDto.Amount;
            var tokenCode = payoutDto.TokenCode;
            var memo = payoutDto.Memo;

            var pmtResponse = await _nexusApiService.CreatePayout(
                accountCode, tokenCode, amount, memo);

            return new TransactionEnvelopeDto(pmtResponse.TransactionEnvelope.SignedTransactionEnvelope);
        }

        public TransactionEnvelopeDto SignTransaction(TransactionEnvelopeDto transactionEnvelopeDto, string signer)
        {
            return
                new TransactionEnvelopeDto(
                    _stellarLogic.SignTransaction(transactionEnvelopeDto.TransactionEnvelope, signer),
                    signingNeeded: false);
        }

        public async Task<HttpResponseMessage> SubmitTransaction(TransactionEnvelopeDto transactionEnvelopeDto)
        {
            return await _nexusApiService.Submit(transactionEnvelopeDto.TransactionEnvelope);
        }
    }
}
