﻿namespace TokenSampleApi.Helpers
{
    public class Tools
    {
        public static string TransformEmailToText(string email)
        {
            return email.Replace("@", "_at_").Replace(".", "_dot_").ToUpperInvariant();
        }
    }
}
