﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IdentityModel.Client;
using Newtonsoft.Json;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Customer;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.TokenSettings;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Account;
using TokenSampleApi.Data.DTOs.Services.NexusApiService;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.Payment;
using TokenSampleApi.Data.DTOs.Services.NexusApiService.TransactionEnvelope;
using TokenSampleApi.Data.DTOs.Repo.Customers;
using TokenSampleApi.Helpers;

namespace TokenSampleApi.Services
{
    public class NexusApiService : INexusApiService
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _clientFactory;

        private readonly string _identityUrl;
        private readonly string _identityId;
        private readonly string _identitySecret;
        private readonly string _paymentMethodCode;

        private string _token = null;
        private DateTime? _expirationTime = null;
        private string _refreshToken = null;

        public NexusApiService(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _configuration = configuration;
            _clientFactory = clientFactory;

            _identityUrl = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-url");
            _identityId = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-id");
            _identitySecret = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-secret");
            _paymentMethodCode = _configuration.GetSection("Nexus").GetValue<string>("payment-method-code") ?? throw new Exception("paymentMethodCode not configured");
        }

        private async Task Register()
        {
            var client = _clientFactory.CreateClient();

            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = $"{_identityUrl}/connect/token",

                ClientId = _identityId,
                ClientSecret = _identitySecret,
                Scope = "api1"
            });

            if (response.IsError) throw new Exception(response.Error);

            _token = response.AccessToken;
            // ExpiresIn - 10% safety margin
            _expirationTime = DateTime.UtcNow.AddSeconds((response.ExpiresIn - (response.ExpiresIn / 10)));
            _refreshToken = response.RefreshToken;
        }

        private async Task Refresh()
        {
            if (string.IsNullOrWhiteSpace(_refreshToken))
            {
                await Register();
            }
            else
            {
                var client = _clientFactory.CreateClient();

                var response = await client.RequestRefreshTokenAsync(new RefreshTokenRequest
                {
                    Address = $"{_identityUrl}/connect/token",

                    ClientId = _identityId,
                    ClientSecret = _identitySecret,

                    RefreshToken = _refreshToken
                });

                if (response.IsError) throw new Exception(response.Error);

                _token = response.AccessToken;
                _expirationTime = DateTime.UtcNow.AddSeconds(response.ExpiresIn);
                _refreshToken = response.RefreshToken;
            }
        }

        private async Task<HttpClient> GetClient()
        {
            if (_token == null)
            {
                await Register();
            }
            else if (_expirationTime != null && _expirationTime.Value < DateTime.UtcNow)
            {
                await Refresh();
            }

            var client = _clientFactory.CreateClient("NexusApiClient");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            client.DefaultRequestHeaders.Add("api_version", "1.2");

            return client;
        }

        private async Task<HttpResponseMessage> GetAsync(string endPoint)
        {
            var client = await GetClient();

            return await client.GetAsync(endPoint);
        }

        private class JsonContent : StringContent
        {
            public JsonContent(object obj) : base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json") { }
        }

        private async static Task<T> ReadContent<T>(HttpResponseMessage response)
        {
            var stringContent = await response.Content.ReadAsStringAsync();

            var content = JsonConvert.DeserializeObject<CustomResultHolder<T>>(stringContent);

            if (response.IsSuccessStatusCode)
            {
                return content.Values;
            }
            else
            {
                var inner = new HttpRequestException(string.Join("\n", content.Errors));
                throw new Exception($"{(int)response.StatusCode}", inner);
            }
        }

        private async Task<HttpResponseMessage> PostJsonAsync(string endPoint, object obj)
        {
            var client = await GetClient();

            return await client.PostAsync(endPoint, new JsonContent(obj));
        }

        public async Task<PagedResult<GetPaymentResponse>> GetPayments(int page = 1, int limit = 50)
        {
            var endPoint = $"token/payments?page={page}&limit={limit}";

            var result = await GetAsync(endPoint);

            return await ReadContent<PagedResult<GetPaymentResponse>>(result);
        }

        public async Task<GetCustomerResponse> GetCustomer(string customerCode)
        {
            var endPoint = $"customer/{customerCode}";

            var encodedEndpoint = System.Web.HttpUtility.UrlEncode(endPoint);

            var result = await GetAsync(encodedEndpoint);

            return result.IsSuccessStatusCode ? await ReadContent<GetCustomerResponse>(result) : null;
        }

        public async Task<PagedResult<GetPaymentResponse>> GetCustomerPayments(string customerCode, int page = 1, int limit = 50)
        {
            var endPoint = $"token/payments?customerCode={customerCode}&page={page}&limit={limit}";

            var result = await GetAsync(endPoint);

            return await ReadContent<PagedResult<GetPaymentResponse>>(result);
        }

        public async Task<PostCustomerResponse> CreateCustomer(string customerCode, string email, string pubKey, string firstName, string lastName, string bankAccountNumber)
        {
            var customerDto = new PostCustomerRequest(customerCode, email, pubKey, firstName, lastName, bankAccountNumber);

            var endPoint = "customer";

            var result = await PostJsonAsync(endPoint, customerDto);

            return await ReadContent<PostCustomerResponse>(result);
        }

        public async Task<GetAccountResponse> GetAccount(string accountCode)
        {
            var endPoint = $"accounts/{accountCode}";

            var result = await GetAsync(endPoint);

            return await ReadContent<GetAccountResponse>(result);
        }

        public async Task<PagedResult<GetPaymentResponse>> GetAccountPayments(string accountCode, int page = 1, int limit = 50)
        {
            var endPoint = $"token/payments?accountCode={accountCode}&page={page}&limit={limit}";

            var result = await GetAsync(endPoint);

            return await ReadContent<PagedResult<GetPaymentResponse>>(result);
        }

        public async Task<PostAccountResponse> CreateAccount(string customerCode)
        {
            var accountDto = new PostAccountRequest();

            var endPoint = $"customer/{customerCode}/accounts";

            var result = await PostJsonAsync(endPoint, accountDto);

            return await ReadContent<PostAccountResponse>(result);
        }

        public async Task<PostPaymentsResponse> CreatePayment(PostPaymentOutputs[] paymentOutputs, string memo = null)
        {
            var endPoint = $"token/payments";

            var paymentDto = new PostPaymentsRequest()
            {
                Payments = paymentOutputs,
                Memo = memo,
            };

            var result = await PostJsonAsync(endPoint, paymentDto);

            return await ReadContent<PostPaymentsResponse>(result);
        }


        public async Task<PostPaymentsResponse> CreateFunding(string customerCode, string accountCode, string tokenCode, decimal amount, string memo = null)
        {
            var endPoint = $"token/fund";

            var paymentDto = new PostFundingRequest()
            {
                AccountCode = accountCode,
                Amount = amount,
                CustomerCode = customerCode,
                Memo = memo,
                TokenCode = tokenCode
            };

            var result = await PostJsonAsync(endPoint, paymentDto);

            return await ReadContent<PostPaymentsResponse>(result);
        }

        public async Task<PostPaymentsResponse> CreatePayout(string accountCode, string tokenCode, decimal amount, string memo = null)
        {
            var endPoint = $"token/payouts";

            var paymentDto = new PostPayoutRequest()
            {
                AccountCode = accountCode,
                Amount = amount,
                Memo = memo,
                PaymentMethodCode = _paymentMethodCode,
                TokenCode = tokenCode
            };

            var result = await PostJsonAsync(endPoint, paymentDto);

            return await ReadContent<PostPaymentsResponse> (result);
        }

        public async Task<HttpResponseMessage> Submit(string transactionEnvelope)
        {
            var endPoint = $"transaction/submit";

            var transactionEnvelopeDto = new TransactionEnvelopeRequest()
            {
                Envelope = transactionEnvelope
            };

            return await PostJsonAsync(endPoint, transactionEnvelopeDto);
        }
    }
}
