# Backend API

### Installation
Tested in Visual Studio 2019.
Open the project in Visual Studio and build it.

### Configuration
The `appsettings.json` file contains the most important information, contact Quantoz Nexus support to obtain the needed configuration to connect to your Nexus Tenant.

Here is an overview of the appsetting properties:
```json
{
  "ConnectionStrings": {
    // If this file does not exist yet, you should create it first
    "DefaultConnection": "Data Source=AspIdUsers.db;"
  },
  "Stellar": {
    "networkUrl": "https://horizon.stellar.org",
    "networkPassPhrase": "Public Global Stellar Network ; September 2015"
  },
  "Nexus": {
    "nexus-api-url": "https://testapi.quantoznexus.com",
    "nexus-identity-url": "https://testidentity.quantoznexus.com",
    // Please checkout: https://testdocs.quantoznexus.com/articles/start-developing/sd_authentication.html
    // to create a client id and secret
    "nexus-identity-id": "{your_client_id}",
    "nexus-identity-secret": "{your_client_secret}",
    // You can checkout your payment methods under: Nexus Portal -> Settings -> Payment Methods tab
    "payment-method-code": "{your SELL payment method code}"
  },
  // This is required for the mobile app to communicate with this api.
  "accepted-anonymous-request-token": "<FILL IN TOKEN USED IN MOBILE CONFIG>"
}
```

Furthermore, the TokenSettings are currently hardcoded in the `/Data/DTOs/Services/NexusApiService/Customer/PostCustomerRequest.cs` file. Talk to our support to verify you have the correct values.

### Testing
To run the TokenSampleAPI locally, run the executable the first time with `/seed` as command line argument to create and/or initialize the database correctly.
