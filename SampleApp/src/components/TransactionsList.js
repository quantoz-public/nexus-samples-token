import React from "react";
import { StyleSheet } from "react-native";
import { List, ListItem, Body, Text, View } from "native-base";
import Transaction from "_components/Transaction";

const styles = StyleSheet.create({
  noTransactionsMessage: {
    marginTop: 32,
    textAlign: "center"
  }
});

const TransactionsList = props => {
  renderList = (
    <View>
      {props.listItems == null || !props.listItems.length ? (
        <ListItem>
          <Body>
            <Text label-light style={styles.noTransactionsMessage}>
              No transactions to show yet.
            </Text>
          </Body>
        </ListItem>
      ) : (
        props.listItems
          .slice(0, props.shortList ? 3 : props.listItems.length)
          .map((x, key) => {
            return (
              <Transaction
                key={key}
                icon="home"
                address={x.mainText == null ? "Placeholder" : x.mainText}
                date={
                  x.comment == null
                    ? null
                    : x.comment.substring(0, 16).replace("T", "  ")
                }
                amount={x.amount}
                loader={x.loader}
                transactionType={x.transactionType}
                memo={x.memo}
              />
            );
          })
      )}
    </View>
  );

  return (
    <List>
      {renderList}
      {props.children}
    </List>
  );
};

export default TransactionsList;
