import React from "react";
import { StyleSheet, View } from "react-native";
import { Text, Button } from "native-base";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import Config from "Config";

const localStyles = StyleSheet.create({
  balanceArea: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 32,
    paddingBottom: 32,
    backgroundColor: "#02182B",
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    marginBottom: 24
  },
  balanceText: {
    paddingTop: 8
  },
  buttonContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingTop: 24
  }
});

export default Balance = props => {
  return (
    <View style={localStyles.balanceArea}>
      <Text label-bold-dark>Balance</Text>
      <LoadingSpinner area="balance-area">
        <Text balance-dark style={localStyles.balanceText}>
          {Config.DEFAULT_ASSET} {props.balance}
        </Text>
      </LoadingSpinner>
      <View style={localStyles.buttonContainer}>
        {!props.user.viewer && (
          <Button
            block
            primary-light
            onPress={() => {
              props.navigation.navigate("Pay", props);
            }}
          >
            <Text button>Pay</Text>
          </Button>
        )}
        <Button
          block
          primary-light
          onPress={() => {
            props.navigation.navigate("ReceiveScreen", props);
          }}
        >
          <Text button>Receive</Text>
        </Button>
      </View>
    </View>
  );
};
