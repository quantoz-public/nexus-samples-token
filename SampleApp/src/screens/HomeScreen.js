import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Linking } from "react-native";
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Content,
  Text,
  Button,
  Icon,
  Left,
  ActionSheet,
  Root
} from "native-base";
import TransactionsList from "_components/TransactionsList";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import { getBalanceAsync, getTransactionsAsync } from "_services/Stellar";
import { signOut } from "_services/Authorization";
import { NavigationActions } from "react-navigation";
import Config from "Config";
import Balance from "_components/Balance";
import { ErrorModal, InfoModal } from "_components/Alert";

export default function HomeScreen({ navigation }) {
  const [transactions, setTransactions] = useState([]);
  const [user, setUser] = useState(
    navigation.getParam("user", { viewer: false })
  );
  const [balance, setBalance] = useState("");
  const [counter, setCounter] = useState(1);
  const [lastTxDate, setLastTxDate] = useState(0);

  useEffect(() => {
    let reload = true;
    if (reload) {
      setUser(navigation.state.params.user);
    }

    // clean-up the promise
    return () => {
      reload = false;
    };
  }, [navigation.state.params.user]);

  useEffect(() => {
    let reload = true;

    const tick = () => {
      if (reload) {
        setCounter(counter + 1);
      }
    };

    var timer = setInterval(tick, 3000);

    // clean-up the promise
    return () => {
      reload = false;
      clearInterval(timer);
    };
  }, [counter]);

  useEffect(() => {
    let reload = true;

    const loadTx = async () => {
      if (transactions == null) {
        var getTx = await trackPromise(
          getTransactionsAsync(user),
          "tx-list-area-A"
        );
      } else {
        var getTx = await getTransactionsAsync(user);
      }

      var txs = getTx.map((x, key) => {
        // isOutgoingTx = TRUE for a pay transaction
        // isOutgoingTx = FALSE for a funding/incoming transaction
        var isOutgoingTx = x.from == user.keypair.publicKey;
        var transactionDirection = isOutgoingTx ? "outgoing" : "incoming";

        return {
          key: key,
          mainText: isOutgoingTx ? x.to : x.from,
          amount: parseFloat(x.amount).toFixed(Config.decimalPrecision),
          comment: x.created,
          transactionType: transactionDirection,
          memo: x.memo
        };
      });

      if (reload) {
        if (transactions == null || transactions.length == 0) {
          setTransactions(txs);
        } else {
          var txDate = Date.parse(txs[0].comment);
          if (txDate > lastTxDate) {
            setTransactions(txs);
            setLastTxDate(txDate);
          }
        }
        const setParamsAction = NavigationActions.setParams({
          params: { transactions: txs, counter },
          key: "TransactionsStack"
        });
        navigation.dispatch(setParamsAction);
      }
    };

    const loadBalance = async () => {
      var userInfo;
      if (balance == "") {
        userInfo = await trackPromise(getBalanceAsync(user), "balance-area");
      } else {
        userInfo = await getBalanceAsync(user);
      }
      if (reload) {
        setBalance(
          parseFloat(userInfo.balance).toFixed(Config.decimalPrecision)
        );
      }
    };

    loadBalance();
    loadTx();

    // clean-up the promise
    return () => {
      reload = false;
    };
  }, [counter]);

  return (
    <Root>
      <SafeAreaView style={{ flex: 1 }}>
        <Container>
          <Balance balance={balance} user={user} navigation={navigation} />
          <Content padder>
            <Text label-bold-light>Last transactions</Text>
            <LoadingSpinner area="tx-list-area-A">
              <TransactionsList listItems={transactions} shortList />
            </LoadingSpinner>
            {transactions.length > 2 ? (
              <Button
                transparent
                block
                button-link
                onPress={() => {
                  navigation.navigate("Transactions", { transactions });
                }}
              >
                <Text button>More transactions</Text>
              </Button>
            ) : null}
          </Content>
        </Container>
      </SafeAreaView>
    </Root>
  );
}

const _signOutAsync = async navigation => {
  try {
    await trackPromise(signOut(), "sign-out-button");
    navigation.navigate("Auth");
  } catch (error) {
    ErrorModal(error);
  }
};

HomeScreen.navigationOptions = ({ navigation }) => {
  let actionSheetOptions = [
    {
      text: "Request demo",
      icon: "mail",
      iconColor: "#183C4E"
    },
    { text: "Credit card", icon: "card", iconColor: "#183C4E" },
    {
      text: "Add new payment method",
      icon: "add-circle",
      iconColor: "#183C4E"
    },
    { text: "Cancel", icon: "close", iconColor: "#183C4E" }
  ];
  const actionSheetCancelOptionIndex = 3;

  const openContactPage = async () => {
    await Linking.openURL("https://quantoz.com/contact/");
  };

  const user = navigation.getParam("user", { viewer: false });

  return {
    header: () => (
      <Header darkTheme noBorderRadiusBottom>
        <Left>
          <Button
            transparent
            onPress={() => {
              InfoModal("disclaimer");
            }}
          >
            <Icon name="information-circle-outline" />
          </Button>
        </Left>
        <Right>
          <Button
            transparent
            onPress={() => {
              ActionSheet.show(
                {
                  options: actionSheetOptions,
                  cancelButtonIndex: actionSheetCancelOptionIndex,
                  title: "Select payment method"
                },
                buttonIndex => {
                  if (buttonIndex != actionSheetCancelOptionIndex) {
                    openContactPage();
                  }
                }
              );
            }}
          >
            <Icon name="add-circle" />
          </Button>
          {!user.viewer && (
            <Button
              transparent
              onPress={() => {
                navigation.navigate("Payout", {
                  user
                });
              }}
            >
              <Icon name="wallet" />
            </Button>
          )}
          <LoadingSpinner area="sign-out-button">
            <Button transparent onPress={() => _signOutAsync(navigation)}>
              <Icon name="log-out" />
            </Button>
          </LoadingSpinner>
        </Right>
      </Header>
    )
  };
};
