// import { authorize, refresh, revoke } from "react-native-app-auth";

import {
  getUserAsync,
  setUserTokenAsync,
  appendUserAsync,
  setUserAsLoggedInAsync,
  getLoggedInUserEmailAsync,
  deleteUserTokenAsync,
  clearUserLoggedInStatusAsync,
  isUserLoggedInAsync,
  setUserItemAsync
} from "_services/Storage";
import {
  authorize,
  refresh,
  revoke,
  registerUser,
  submitTransactionAsync
} from "_services/Server";
import { authConfig } from "Config";
import { Linking } from "react-native";
import StellarBase from "stellar-base";
import { signTransaction } from "_services/Stellar";

const verifyAuthData = async authdata => {
  if (authdata.accessToken == null) {
    throw new Error("No accessToken retreived");
  }

  if (authdata.expiresIn == null) {
    throw new Error("No accessToken expiry retreived");
  }

  if (authdata.refreshToken == null) {
    throw new Error("No refreshToken retreived");
  }
};

var refreshPending = false;
const refreshTokenAsync = async user => {
  try {
    if (refreshPending == false) {
      console.log("Refresh pending");
      refreshPending = true;
      if (user != null && typeof user.token != "undefined") {
        var authdata = await refresh(authConfig, {
          refreshToken: user.token.refreshToken
        });

        verifyAuthData(authdata);

        // Add ~1 hour expiration time (3000s)
        authdata.tokenExpirationTime = Date.now() + 3000 * 1000;

        console.log("refresh bearer token");
        await setUserTokenAsync(user, authdata);
        console.log("authData", authdata);
      }

      refreshPending = false;
    }
  } catch (e) {
    refreshPending = false;
    signOut();
  }
};

const revokeTokenAsync = async token => {
  const result = await revoke(authConfig, {
    tokenToRevoke: token,
    sendClientId: true
  });
  console.log("RevokeToken", result);
};

export const createAccountAsync = async (
  email,
  password,
  firstName,
  lastName,
  bankAccountNumber
) => {
  if (email == null || email == "") {
    throw Error("Email not filled in");
  }

  if (password == null || password == "") {
    throw Error("Password not filled in");
  }

  // does user already exists in the Async Storage?
  var user = await getUserAsync(email);
  if (user != null) {
    throw Error("User with this email already exists");
  }

  // create Stellar keypair as crypto account
  var keypair = StellarBase.Keypair.random();

  // create Nexus customer with created keyPair
  // + account with Config.DEFAULT_TOKEN by default
  var response = await registerUser({
    email,
    password,
    cryptoAddress: keypair.publicKey(),
    firstName,
    lastName,
    bankAccountNumber
  });
  console.log("RESPONSE", response);

  // store Stellar keypair in Async Storage
  // !! this is unsafe !!
  // see https://github.com/FormidableLabs/react-native-app-auth/blob/master/docs/token-storage.md
  console.log("Warning: Storing Crypto Wallet in Async Storage!");
  console.log("KP", keypair.publicKey());
  var value = { publicKey: keypair.publicKey(), secret: keypair.secret() };
  await setUserItemAsync(email, "keypair", value);
  // get user because keypair is used
  var user = await getUserAsync(email);

  console.log("start submit");
  const signedTx = signTransaction(
    response.transactionEnvelope,
    user.keypair.secret
  );
  console.log("Submitting tx:", signedTx);

  // log-in to be able to submit the Tx
  await logInAsync(email, password);
  user = await authorizeAsync();

  await submitTransactionAsync(user, signedTx);
  console.log("USER", user);
};

export const logInAsync = async (email, password) => {
  if (email == null || email == "") {
    throw Error("Email not filled in");
  }

  if (password == null || password == "") {
    throw Error("Password not filled in");
  }

  // first log-out possible logged-in user
  if (await isUserLoggedInAsync()) {
    console.log("Sign out logged in user");
    await signOut();
  }

  // does user exists in the Async Storage?
  // only allow logging in if a keyPair exists
  var user = await getUserAsync(email);
  if (user == null || user.keypair == null || user.keypair.publicKey == null) {
    throw Error("Account does not exist on phone. Please sign up first");
  }

  // get token
  var authdata = await authorize(authConfig, { user: email, password });

  verifyAuthData(authdata);

  // Add ~1 hour token expiration time (3000s)
  authdata.tokenExpirationTime = Date.now() + 3000 * 1000;

  // store user in Async Storage
  // !! this is unsafe !!
  // see https://github.com/FormidableLabs/react-native-app-auth/blob/master/docs/token-storage.md
  console.log("Warning: Storing Token in Async Storage!");
  await appendUserAsync(email, {
    token: authdata
  });

  console.log("Set user as 'logged in'");
  await setUserAsLoggedInAsync(email);
};

// check authorization token and refresh if needed
export const authorizeAsync = async () => {
  if (!(await isUserLoggedInAsync())) {
    throw new Error("1. User is not logged in");
  } else {
    var email = await getLoggedInUserEmailAsync();
    var user = await getUserAsync(email);

    if (
      typeof user.token == "undefined" ||
      typeof user.token.tokenExpirationTime == "undefined"
    ) {
      throw new Error("2. User is not logged in");
    } else if (user.token.tokenExpirationTime < Date.now()) {
      !refreshPending &&
        console.log("Token expired, refresh", user.token.tokenExpirationTime);
      await refreshTokenAsync(user);
    }

    // return logged-in user
    return await getUserAsync(email);
  }
};

export const signOut = async () => {
  try {
    var email = await getLoggedInUserEmailAsync();
    var user = await getUserAsync(email);
    await deleteUserTokenAsync(email);

    await revokeTokenAsync(user.token.refreshToken);
    await revokeTokenAsync(user.token.accessToken);
  } catch (error) {
    // catch here to just continue log-out
    console.log(error);
  } finally {
    await clearUserLoggedInStatusAsync();
  }
};
